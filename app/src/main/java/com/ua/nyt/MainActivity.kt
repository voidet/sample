package com.ua.nyt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val articleList = findViewById<RecyclerView>(R.id.article_list)
        val articleViewModel: ArticleViewModel by viewModels()
        val articleAdapter = ArticleAdapter()
        articleViewModel.articleList.observe(this, articleAdapter)
        articleList.adapter = articleAdapter
        articleList.layoutManager = LinearLayoutManager(this)
    }
}
