package com.ua.nyt

import android.util.JsonReader
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class ArticleViewModel : ViewModel() {
    val articleList : MutableLiveData<List<ArticleData>> by lazy {
        MutableLiveData<List<ArticleData>>().also {
            fetchArticles()
        }
    }

    private fun fetchArticles() {
        viewModelScope.launch(Dispatchers.IO) {
            val url = URL("https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=d31fe793adf546658bd67e2b6a7fd11a")
            val connection = (url.openConnection() as? HttpsURLConnection)
            val fetchedArticles = ArrayList<ArticleData>(100)
            try {
                connection?.run {
                    readTimeout = 3000
                    connectTimeout = 3000
                    requestMethod = "GET"
                    doInput = true
                    if (responseCode != HttpsURLConnection.HTTP_OK) {
                        throw IOException("HTTP error code: $responseCode")
                    }
                    inputStream?.let { stream ->
                        val reader = JsonReader(BufferedReader(InputStreamReader(stream)))
                        reader.beginObject()
                        var status: String
                        while (reader.hasNext()) {
                            when (reader.nextName()) {
                                "status" -> {
                                    status = reader.nextString()
                                    println("status $status")
                                }
                                "response" -> {
                                    reader.beginObject()
                                    while (reader.hasNext()) {
                                        when (reader.nextName()) {
                                            "docs" -> {
                                                reader.beginArray()
                                                while (reader.hasNext()) {
                                                    reader.beginObject()
                                                    while (reader.hasNext()) {
                                                        when (reader.nextName()) {
                                                            "headline" -> {
                                                                reader.beginObject()
                                                                while (reader.hasNext()) {
                                                                    when (reader.nextName()) {
                                                                        "main" -> {
                                                                            fetchedArticles.add(
                                                                                ArticleData(
                                                                                    reader.nextString()
                                                                                )
                                                                            )
                                                                        }
                                                                        else -> reader.skipValue()
                                                                    }
                                                                }
                                                                reader.endObject()
                                                            }
                                                            else -> reader.skipValue()
                                                        }
                                                    }
                                                    reader.endObject()
                                                }
                                                reader.endArray()
                                            }
                                            else -> reader.skipValue()
                                        }
                                    }
                                    reader.endObject()
                                }
                                else -> reader.skipValue()
                            }
                        }
                        reader.endObject()
                    }
                }
            } finally {
                connection?.disconnect()
                launch(Dispatchers.Main){ articleList.value = fetchedArticles }
            }
        }
    }
}