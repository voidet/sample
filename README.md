# NYT Live Coding Challenge
## Instructions
You will work with your interviewers to implement as many new features as you can. Work with them as you would pair programming with a coworker.
You are not expected to get through most of features. Finishing one is good.

As you are working, build a familiarity with the codebase with a critical eye. You will answer questions at the end of the interview about code quality and improvements.

## App Overview
This simple app fetches a list of articles from the New York Times API and displays them in a
RecyclerView. The app is written in Kotlin and uses a ViewModel, LiveData, and Coroutines.

![Home Screen](docs/screens/home_screen.png)

## New Features
1. Open the article in a browser or WebView when clicked.
2. Add a button to refresh the list.
3. Add a search bar to search for articles.
4. Add thumbnail previews for articles.
5. Fetch the next page of results when a user scrolls to the end of the list.

## Questions
1. What did you like about the codebase?
2. In what ways would you improve the codebase?
3. For the features that you did not complete, how would have you approached implementing them?

## Helpful Links
* [NYT Article Search API Docs](https://developer.nytimes.com/docs/articlesearch-product/1/routes/articlesearch.json/get)
* [Android Jetpack](https://developer.android.com/jetpack)
